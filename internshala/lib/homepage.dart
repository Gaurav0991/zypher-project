import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        child: Stack(
          children:<Widget>[
            Positioned(
              left: MediaQuery.of(context).size.width*0.07,
              top: MediaQuery.of(context).size.height*0.10,
              child: Text("Lets get you Hooked Up!",
                style: TextStyle(fontWeight:FontWeight.bold,fontSize: 15,letterSpacing: 0.7),),) ,
          
            Positioned(
              left: MediaQuery.of(context).size.width*0.05,
              top: MediaQuery.of(context).size.height*0.14,
              child: Text("Benefits includes:",
                style: TextStyle(fontWeight:FontWeight.w500,fontSize: 15,letterSpacing: 0.7,color: Colors.lightBlue),),),
            Positioned(
              left: MediaQuery.of(context).size.width*0.051,
              top: MediaQuery.of(context).size.height*0.18,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:<Widget>[
                  _buildElement(context,"All Locations",Icons.location_on,"Connect through any 97 cities",Colors.green),
                  SizedBox(height:15),
                  _buildElement(context, "Top Speed", Icons.signal_cellular_4_bar, "Don't left safety in the way of enjoying media", Colors.pink),
                  SizedBox(height:17),
                  _buildElement(context, "No Ads", FontAwesomeIcons.bullhorn, "Get rid of all those banner and videos", Colors.purple),                  
                                  ]
                                ),
                              ),
                              Positioned(
                                top: MediaQuery.of(context).size.height*0.45,
                                left: MediaQuery.of(context).size.width*0.35,
                                child:Text("Choose a Plan",style: TextStyle(color:Colors.grey,letterSpacing:3.5,fontWeight:FontWeight.bold,fontSize: 13),) 
                              ),
                              Positioned(
                                left: 20,
                                right: 20 ,
                                top: MediaQuery.of(context).size.height*0.50,
                                child: GestureDetector(
                                  onTap: (){
                                    String url='https://backend-test-zypher.herokuapp.com/ebooks/activatePlan';
                                    http.post(url,body: {
                                      "planId":"6 months"
                                    }).then((response){
                                      print(response.body);
                                      var data=jsonDecode(response.body);
                                      _showDialogBox(context,data['status']);
                                    });
                                  },
                                    child: Card(
                                    elevation: 2,
                                    color: Colors.pinkAccent[100],
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children:<Widget>[
                                            Icon(Icons.check,color:Colors.lightGreenAccent[700],size: 35,),
                                            SizedBox(width:12),
                                            Expanded(child:Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children:<Widget>[
                                                  Text("6 Months Member Ship",style: TextStyle(fontWeight:FontWeight.bold,letterSpacing:2.5,fontSize: 17),),
                                                  SizedBox(height:1),
                                                  Text("Valid for 6 months from the date of Purchase",style: TextStyle(fontWeight:FontWeight.w400,letterSpacing:2.5,fontSize: 14),)

                                                ]
                                              ),
                                            )
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(right:8.0),
                                              child: Text("\$29"),
                                            ),
                                        ]
                                      ),
                                    ),
                                  ),
                                )),
                             Positioned(
                                left: 20,
                                right: 20 ,
                                top: MediaQuery.of(context).size.height*0.65,
                                child: GestureDetector(
                                  onTap: (){
                                    String url='https://backend-test-zypher.herokuapp.com/ebooks/activatePlan';
                                    http.post(url,body: {
                                      "planId":"12 months"
                                    }).then((response)async{
                                      print(response.body);
                                      var data=jsonDecode(response.body);
                                      _showDialogBox(context,data['status']);
                                    });
                                  },
                                                                  child: Card(
                                    elevation: 2,
                                    color: Colors.white,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children:<Widget>[
                                           
                                            SizedBox(width:12),
                                            Expanded(child:Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children:<Widget>[
                                                  Text("12 Months Member Ship",style: TextStyle(fontWeight:FontWeight.bold,letterSpacing:2.5,fontSize: 17),),
                                                  SizedBox(height:1),
                                                  Text("Valid for 6 months from the date of Purchase.",style: TextStyle(fontWeight:FontWeight.w400,letterSpacing:2.5,fontSize: 14),)

                                                ]
                                              ),
                                            )
                                            ),
                                         
                                        ]
                                      ),
                                    ),
                                  ),
                                )),
                                Positioned(
                                  bottom: 50,
                                  left: MediaQuery.of(context).size.width*0.15,
                                  width: 290,
                                  child: RaisedButton(
                                    color: Colors.greenAccent[700],
                                  shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.lightGreenAccent[400])
                                  ),
                                  child: Text("Activate @ \$29",style: TextStyle(color: Colors.white,fontWeight:FontWeight.w500,fontSize:22),),  
                                  onPressed: (){

                                  },
                                  )
                                )
                            
                            ]
                          ),
                        )
                      );
                    }
                  
                    _buildElement(BuildContext context, String title, IconData iconData, String subtitile, MaterialColor color) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                        Icon(iconData,color: color,size: 30,),
                        SizedBox(width: 15  ,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                          
                          Text(title,style: TextStyle(fontWeight:FontWeight.bold,fontSize: 15),),
                          Text(subtitile)
                        ],)
                      ],);
                    }
                    Future<void> _showDialogBox(BuildContext context,msg) async {
                    return showDialog<void>(
                    context: context,
                    barrierDismissible: true, // user must tap button!
                    builder: (BuildContext context) {
                    return AlertDialog(
                    title: Text('Response'),
                    content: SingleChildScrollView(
                    child: ListBody(
                    children: <Widget>[
                    Text('$msg'),
                    ],
                    ),
                    ),
                    actions: <Widget>[
                    FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                    Navigator.of(context).pop();
                    },
                    ),
                    ],
                    );
                    },
                    );
                    }
}